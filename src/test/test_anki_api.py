import time
from dataclasses import dataclass

import pytest

from src.main.anki_api import NewNoteOptions, NewNoteFields, AnkiNewNote, AnkiConnectAPI, DuplicateScopeOptions, \
    AnkiNote

api = AnkiConnectAPI()  # TODO: make session fixture


@dataclass
class Deck:
    name: str
    id: int


def add_notes(notes_count: int, deck_name: str) -> list[int]:
    # Get deck notes before
    deck_notes_before = api.find_notes(query=f'deck:"{deck_name}"')

    # Create notes stabs
    notes = []
    for num in range(notes_count):
        note = create_note_stub(front=f'front {num}', back=f'back {num}', deck=deck_name)
        notes.append(note)

    # Add new notes
    notes_ids = api.add_notes(notes=notes)

    # Check notes added
    assert notes_ids
    deck_notes_after = api.find_notes(query=f'deck:"{deck_name}"')
    exp_notes_count = len(deck_notes_before) + len(notes_ids)

    assert set(notes_ids).issubset(set(deck_notes_after)), f"Some note not added: " \
                                                           f"{set(deck_notes_after).difference(set(notes_ids))}"
    assert len(deck_notes_after) == exp_notes_count, f"Expected notes count: {exp_notes_count}, " \
                                                     f"received: {len(deck_notes_after)}"

    return notes_ids


def create_note_stub(front: str, back: str, deck: str) -> AnkiNewNote:
    """
    Create Anki note stub.
    :param front: Front content.
    :param back: Back content.
    :param deck: Deck name.
    :return: AnkiNote.
    """
    options = NewNoteOptions(allow_duplicate=False,
                             duplicate_scope='deck',
                             duplicate_scope_options=DuplicateScopeOptions(deck_name='Default',
                                                                           check_children=False,
                                                                           check_all_models=False))
    note = AnkiNewNote(deck_name=deck,
                       fields=NewNoteFields(front=front, back=back),  # TODO: make random, add "[Autotest]" marker
                       options=options, picture=[], video=[], audio=[], tags=[])
    return note


def check_note(note: AnkiNewNote, note_info: AnkiNote) -> None:
    assert note.model_name == note_info.model_name
    assert note.fields.back == note_info.fields.back.value
    assert note.fields.front == note_info.fields.front.value
    assert note.tags == note_info.tags


class TestAnkiConnectAPI:
    test_deck_name = '[Autotest TestAnkiConnectAPI] Deck'

    @pytest.fixture(scope='function')
    def test_deck(self) -> Deck:
        """
        [Prepare] Create empty test deck.
        :return: Test deck.
        """
        test_deck_name = '[Autotest TestAnkiConnectAPI] Deck'
        test_deck_id = api.create_deck(deck_name=test_deck_name)
        yield Deck(name=test_deck_name, id=test_deck_id)
        # clean up
        api.delete_decks(decks_names=[test_deck_name])  # after each tests in class

    def test_deck_actions(self):
        """Check create_deck, get_decks and delete_decks"""
        test_deck_name = self.test_deck_name
        # Delete test deck if already exist
        api.delete_decks(decks_names=[test_deck_name])
        decks_before = api.get_deck_names_and_ids()

        # 1. Create deck
        test_deck_id = api.create_deck(deck_name=test_deck_name)
        assert test_deck_id
        # 2. Get deck
        decks_after_create = api.get_deck_names_and_ids()

        # Check deck created
        new_deck_id = decks_after_create.pop(test_deck_name)
        assert new_deck_id == test_deck_id
        # check odl deck not changed
        assert decks_before == decks_after_create

        # Delete test deck
        api.delete_decks(decks_names=[test_deck_name])

        # Check there is no test_deck in Anki
        decks_after_del = api.get_deck_names_and_ids()
        deleted_deck = decks_after_del.get(test_deck_id)
        assert not deleted_deck
        # check odl deck not changed
        assert decks_before == decks_after_create

    def test_notes_action(self, test_deck: Deck):
        """Check add_notes, notes_info and delete_notes"""
        test_deck_name = test_deck.name

        # Create notes stub
        test_notes = [create_note_stub(front='note1 front', back='note1 back', deck=test_deck_name),
                      create_note_stub(front='note2 front', back='note2 back', deck=test_deck_name)]
        # 1. Add notes
        notes_ids = api.add_notes(notes=test_notes)

        # Check ids exists
        assert notes_ids

        # 2. Check [notes_info] notes created in Anki
        notes_info = api.notes_info(ids=notes_ids)
        assert len(test_notes) == len(notes_info), f'Expected created notes count: {len(test_notes)},' \
                                                   f'received: {len(notes_info)}'
        for test_note, note_info in zip(test_notes, notes_info):
            check_note(note=test_note, note_info=note_info)

        # 3. Delete notes
        api.delete_notes(ids=notes_ids)

        # Check there are no notes in Anki after delete
        notes_info = api.notes_info(ids=notes_ids)
        for note_info in notes_info:
            assert not note_info

    def test_add_note(self, test_deck):
        test_deck_name = test_deck.name

        # Create notes stub
        test_note = create_note_stub(front='note1 front', back='note1 back', deck=test_deck_name)
        # Add notes
        note_id = api.add_note(note=test_note)

        # Check ids exists
        assert note_id

        # Check notes created in Anki
        notes_info = api.notes_info(ids=[note_id])
        assert len(notes_info) == 1, f'Expected created notes count: 1, received: {len(notes_info)}'
        note_info = notes_info[0]
        check_note(note=test_note, note_info=note_info)

        # Clean: delete notes
        api.delete_notes(ids=[note_id])

    def test_change_deck(self):
        deck1_name = "Test Deck 1"
        deck2_name = "Test Deck 2"
        deck1_id = api.create_deck(deck_name=deck1_name)
        deck2_id = api.create_deck(deck_name=deck2_name)

        note = create_note_stub(front='1 fronet', back='1 backe', deck=deck1_name)
        note_id = api.add_note(note=note)

        api.change_deck(deck_name=deck2_name, cards=[note_id])
        decks_data = api.get_decks([note_id])
        decks_with_card = list(decks_data.keys())

        assert deck2_name in decks_with_card
        assert deck1_name not in decks_with_card

        # Clean: delete decks
        api.delete_decks(decks_names=[deck1_name, deck2_name], cards_too=True)

    def test_find_cards(self, test_deck):
        # 1. Add notes [and its cards]
        notes_ids = add_notes(notes_count=2, deck_name=test_deck.name)
        # Get deck stats
        decks_stats = api.get_decks_stats(decks=[test_deck.name])
        deck_stats = decks_stats.get(str(test_deck.id))
        # Ged deck cards IDs
        cards_ids = api.find_cards(query=f'deck:"{test_deck.name}"')
        # Check cards count compare with deck stats cards count
        exp_cards_count = deck_stats.get('total_in_deck')
        assert len(cards_ids) == exp_cards_count, f'Expected cards count: {exp_cards_count}, received: {len(cards_ids)}'


# def get_decks(retry):
#     decks_data = api.get_decks([note_id])
