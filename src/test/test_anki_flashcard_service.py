from random import choice

import pytest

from src.main.anki_api import AnkiConnectAPI
from src.main.flashcard_service.anki.anki_connect import AnkiConnectService
from src.main.flashcard_service.anki.anki_flashcard_service import AnkiFlashcardService
from src.main.flashcard_service.models import NewFlashCard, ExistedFlashCard
from src.test.test_anki_api import api, create_note_stub

anki_api = AnkiConnectAPI()


class TestAnkiFlashcardService:
    test_lexicon_deck_name = 'Lexicon Autotest TestAnkiFlashcardService'
    test_film_deck_name = 'FilmDeck Autotest TestAnkiFlashcardService'

    # def teardown_method(self):
    #     # delete lexicon deck notes
    #     test_lexicon_notes_ids = anki_api.find_notes(query=f'deck:"{self.test_lexicon_deck_name}"')
    #     anki_api.delete_notes(ids=test_lexicon_notes_ids)
    #     # delete film deck notes
    #     test_film_notes_ids = anki_api.find_notes(query=f'deck:"{self.test_film_deck_name}"')
    #     anki_api.delete_notes(ids=test_film_notes_ids)

    @pytest.fixture(scope='function')
    def anki_fsh_service(self) -> AnkiFlashcardService:
        anki_fsh = AnkiFlashcardService(film_deck_name=self.test_film_deck_name,
                                        lexicon_deck_name=self.test_lexicon_deck_name)
        yield anki_fsh
        anki_api.delete_decks(decks_names=[self.test_film_deck_name, self.test_lexicon_deck_name])

    @pytest.fixture(scope='class', autouse=True)
    def anki_connect(self) -> AnkiConnectService:
        anki_connect_service = AnkiConnectService()
        return anki_connect_service

    def test_get_non_lexicon_words(self, anki_fsh_service):
        # Prepare
        all_words = ['word1', 'word2', 'word3', 'word4', 'word5']  # 5 unique words
        exp_lexicon_words = [choice(all_words) for _ in range(2)]  # 2 words
        exp_non_lexicon_words = [word for word in all_words if word not in exp_lexicon_words]  # 3 words

        # Create words notes stub
        test_notes = [create_note_stub(front=word, back=f'{word} back', deck=anki_fsh_service.lexicon_deck.name)
                      for word in exp_lexicon_words]
        # Add notes with exp_lexicon_words
        notes_ids = api.add_notes(notes=test_notes)

        # Action: get non_lexicon_words
        non_lexicon_words = anki_fsh_service.get_non_lexicon_words(words=set(all_words))

        # Check non_lexicon_words
        assert non_lexicon_words == set(exp_non_lexicon_words), f'Expected non_lexicon_words={exp_non_lexicon_words},' \
                                                                f' received: {non_lexicon_words}'

    def test_add_new_lexicon_flashcards(self, anki_fsh_service, anki_connect):
        # Prepare
        # get lexicon deck notes before
        lexicon_deck = anki_fsh_service.lexicon_deck
        lexicon_notes_before = anki_connect.get_deck_notes(lexicon_deck)
        # dict: words and its flashcard
        lexicon_flashcards_before = anki_connect.notes2flashcards(notes=lexicon_notes_before)

        # create new notes stubs
        new_flashcards = [NewFlashCard(word='new word1', meaning='word1 meaning'),
                          NewFlashCard(word='new word2', meaning='word2 meaning')]

        # Action: add_new_lexicon_flashcards
        anki_fsh_service.add_new_lexicon_flashcards(flashcards=new_flashcards)

        # Checks
        # get lexicon deck notes after
        lexicon_notes_after = anki_connect.get_deck_notes(lexicon_deck)
        # dict: words and its flashcard
        lexicon_flashcards_after = dict([(note.fields.front.value, anki_connect.note2flashcard(note=note))
                                         for note in lexicon_notes_after])

        exp_flashcards_count = len(lexicon_flashcards_before) + len(new_flashcards)
        flashcards_count = len(lexicon_flashcards_after)
        # check flashcards count after new words added
        assert flashcards_count == exp_flashcards_count, f'Expected flashcards count {exp_flashcards_count}, ' \
                                                         f'received {flashcards_count}.'
        # check new flashcards
        for exp_flashcard in new_flashcards:
            exp_word = exp_flashcard.word
            new_flashcard = lexicon_flashcards_after.get(exp_word)
            assert new_flashcard, f'New flashcard for word={exp_word} not added. Flashcard: {exp_flashcard}'
            compare_flashcards(exp_flashcard=exp_flashcard, flashcard=new_flashcard)

        # check old flashcards
        for exp_flashcard in lexicon_flashcards_before:
            exp_word = exp_flashcard.word
            old_flashcard = lexicon_flashcards_after.get(exp_word)
            assert old_flashcard, f'Old flashcard for word={exp_word} not found. Flashcard: {exp_flashcard}'
            compare_flashcards(exp_flashcard=exp_flashcard, flashcard=old_flashcard)

    def test_move_lexicon_words_to_film_deck(self, anki_fsh_service, anki_connect):
        # Prepare
        all_lexicon_words = ['word1', 'word2', 'word3', 'word4', 'word5']  # 5 unique words
        exp_film_words = set([choice(all_lexicon_words) for _ in range(2)])  # 2 words
        exp_non_film_lexicon_words = [word for word in all_lexicon_words if word not in exp_film_words]  # 3 words

        # add words notes to lexicon
        lexicon_deck = anki_fsh_service.lexicon_deck
        add_notes_by_words(words=all_lexicon_words, deck_name=lexicon_deck.name)

        # get lexicon deck flashcards before
        lexicon_notes_before = anki_connect.get_deck_notes(lexicon_deck)
        lexicon_flashcards_before = dict([(note.fields.front.value, anki_connect.note2flashcard(note=note))
                                          for note in lexicon_notes_before])

        # Action: move_lexicon_words_to_film_deck
        anki_fsh_service.move_lexicon_words_to_film_deck(words=exp_film_words)

        # get lexicon deck flashcards after
        lexicon_notes_after = anki_connect.get_deck_notes(lexicon_deck)
        lexicon_flashcards_after = dict([(note.fields.front.value, anki_connect.note2flashcard(note=note))
                                         for note in lexicon_notes_after])

        # get film flashcards
        film_deck = anki_fsh_service.film_deck
        film_notes = anki_connect.get_deck_notes(film_deck)
        film_flashcards = dict([(note.fields.front.value, anki_connect.note2flashcard(note=note))
                                for note in film_notes])

        # check film flashcards count
        exp_film_flashcards_count = len(exp_film_words)
        film_flashcards_count = len(film_flashcards)
        # check flashcards count after new words added
        assert film_flashcards_count == exp_film_flashcards_count, f'Expected film flashcards count ' \
                                                                   f'{exp_film_flashcards_count}, ' \
                                                                   f'received {film_flashcards_count}.'
        # check non film lexicon flashcards
        for exp_film_word in exp_film_words:
            lexicon_flashcard = lexicon_flashcards_before.get(exp_film_word)
            film_flashcard = film_flashcards.get(exp_film_word)
            assert film_flashcard, f'New flashcard for word={exp_film_word} not added. Flashcard: {lexicon_flashcard}'
            compare_flashcards(exp_flashcard=lexicon_flashcard, flashcard=film_flashcard)

        # check film flashcards count
        exp_non_film_flashcards_count = len(exp_non_film_lexicon_words)
        lexicon_flashcards_count = len(lexicon_flashcards_after)
        # check flashcards count after new words added
        assert lexicon_flashcards_count == exp_non_film_flashcards_count, f'Expected non film lexicon flashcards count' \
                                                                          f' {exp_non_film_flashcards_count}, ' \
                                                                          f'received {lexicon_flashcards_count}.'
        # check non film lexicon flashcards
        for non_film_word in exp_non_film_lexicon_words:
            exp_lexicon_flashcard = lexicon_flashcards_before.get(non_film_word)
            lexicon_flashcard = lexicon_flashcards_after.get(non_film_word)
            assert lexicon_flashcard, f'Old lexicon flashcard for word={non_film_word} not found. ' \
                                      f'Flashcard: {exp_lexicon_flashcard}'
            compare_flashcards(exp_flashcard=exp_lexicon_flashcard, flashcard=lexicon_flashcard)

    def test_merge_film_deck_to_lexicon(self, anki_fsh_service, anki_connect):
        # Prepare
        # non empty lexicon deck
        lexicon_words = ['word1', 'word4']  # 2 unique words
        lexicon_deck = anki_fsh_service.lexicon_deck
        add_notes_by_words(words=lexicon_words, deck_name=lexicon_deck.name)
        lexicon_notes_before = anki_connect.get_deck_notes(lexicon_deck)
        lexicon_flashcards_before = anki_connect.notes2flashcards(notes=lexicon_notes_before)

        # non empty film deck
        film_words = ['word2', 'word3', 'word5']  # 3 unique words
        film_deck = anki_fsh_service.film_deck
        add_notes_by_words(words=film_words, deck_name=film_deck.name)
        film_notes_before = anki_connect.get_deck_notes(film_deck)
        film_flashcards_before = anki_connect.notes2flashcards(notes=film_notes_before)

        # Action:
        anki_fsh_service.merge_film_deck_to_lexicon()

        # Checks
        lexicon_notes_after = anki_connect.get_deck_notes(lexicon_deck)
        lexicon_flashcards_after = dict([(note.fields.front.value, anki_connect.note2flashcard(note=note))
                                         for note in lexicon_notes_after])
        film_notes_after = anki_connect.get_deck_notes(film_deck)
        film_flashcards_after = dict([(note.fields.front.value, anki_connect.note2flashcard(note=note))
                                      for note in film_notes_after])

        # check lexicon flashcards count
        exp_lexicon_flashcards_count = len(film_flashcards_before) + len(lexicon_flashcards_before)
        lexicon_flashcards_count = len(lexicon_flashcards_after)
        # check flashcards count after new words added
        assert lexicon_flashcards_count == exp_lexicon_flashcards_count, f'Expected lexicon flashcards count' \
                                                                         f' {exp_lexicon_flashcards_count}, ' \
                                                                         f'received {lexicon_flashcards_count}.'
        # check no film flashcards
        film_flashcards_count = len(film_flashcards_after)
        assert not film_flashcards_count, f'Expected film lexicon is empty, but flashcards received: ' \
                                          f'{film_flashcards_after} '  # TODO: to str

        # check lexicon flashcards
        exp_lexicon_flashcards = lexicon_flashcards_before + film_flashcards_before
        for exp_lexicon_flashcard in exp_lexicon_flashcards:
            exp_word = exp_lexicon_flashcard.word
            lexicon_flashcard = lexicon_flashcards_after.get(exp_word)
            assert lexicon_flashcard, f'Expected flashcard for word={exp_word} added not found in lexicon. ' \
                                      f'Flashcard: {exp_lexicon_flashcard.__dict__}'  # TODO: to str
            compare_flashcards(exp_flashcard=exp_lexicon_flashcard, flashcard=lexicon_flashcard)

        # check film deck deleted
        film_deck_id = anki_connect.get_deck_id(deck_name=film_deck.name)
        assert not film_deck_id, f'Expected film deck deleted^ but film deck ({film_deck.name})' \
                                 f' exist by id={film_deck_id}'

    def test_create_lexicon_deck(self, anki_connect):
        # Prepare
        lexicon_deck_name = self.test_lexicon_deck_name
        lexicon_deck_before = anki_connect.get_deck_id(deck_name=lexicon_deck_name)
        assert not lexicon_deck_before, f'Expected lexicon deck ({lexicon_deck_name}) not exist, ' \
                                        f'but its exists by id={lexicon_deck_before}'
        # Action: start_anki_fsh_service
        anki_fsh_service = AnkiFlashcardService(lexicon_deck_name=lexicon_deck_name,
                                                film_deck_name=self.test_film_deck_name)
        lexicon_deck_after = anki_connect.get_deck_id(deck_name=lexicon_deck_name)
        assert lexicon_deck_after, f'Expected lexicon deck ({lexicon_deck_name}) exist, but its not exist.'

        # Clean Up: remove anki_fsh_service test decks
        anki_api.delete_decks(decks_names=[self.test_film_deck_name, self.test_lexicon_deck_name])

    def test_create_film_deck(self, anki_connect):
        # Prepare
        film_deck_name = self.test_film_deck_name
        film_deck_before = anki_connect.get_deck_id(deck_name=film_deck_name)
        assert not film_deck_before, f'Expected film deck ({film_deck_name}) not exist, ' \
                                     f'but its exists by id={film_deck_before}'
        # Action start_anki_fsh_service
        anki_fsh_service = AnkiFlashcardService(lexicon_deck_name=self.test_lexicon_deck_name,
                                                film_deck_name=film_deck_name)
        film_deck_after = anki_connect.get_deck_id(deck_name=film_deck_name)
        assert film_deck_after, f'Expected film deck ({film_deck_after}) exist, but its not exist.'

        # Clean Up: remove anki_fsh_service test decks
        anki_api.delete_decks(decks_names=[self.test_film_deck_name, self.test_lexicon_deck_name])


def compare_flashcards(exp_flashcard: ExistedFlashCard | NewFlashCard, flashcard: ExistedFlashCard) -> None:
    assert flashcard.word == exp_flashcard.word, f'Expected word={exp_flashcard.word}, received: {flashcard.word}'
    assert flashcard.meaning == exp_flashcard.meaning, f'Expected word={exp_flashcard.meaning}, ' \
                                                       f'received: {flashcard.meaning}'
    assert flashcard.card_id, f'Expected card_id not em[ty, but card_id={flashcard.card_id}'
    assert flashcard.sub_cards_ids, f'Expected sub_cards_ids not em[ty, but sub_cards_ids={flashcard.sub_cards_ids}'


def add_notes_by_words(words: list[str], deck_name: str) -> list[int]:
    # Get deck notes before
    deck_notes_before = api.find_notes(query=f'deck:"{deck_name}"')

    # Create notes stabs
    notes = []
    for word in words:
        note = create_note_stub(front=word, back=f'{word} meaning', deck=deck_name)
        notes.append(note)

    # Add new notes
    notes_ids = api.add_notes(notes=notes)

    # Check notes added
    assert notes_ids
    deck_notes_after = api.find_notes(query=f'deck:"{deck_name}"')
    exp_notes_count = len(deck_notes_before) + len(notes_ids)

    assert set(notes_ids).issubset(set(deck_notes_after)), f"Some note not added: " \
                                                           f"{set(deck_notes_after).difference(set(notes_ids))}"
    assert len(deck_notes_after) == exp_notes_count, f"Expected notes count: {exp_notes_count}, " \
                                                     f"received: {len(deck_notes_after)}"

    return notes_ids
