import pytest

from src.main.nlp_service.nltk_nlp_service import NLPService


class TestSRTSubtitleParser:

    @pytest.fixture(scope='class')
    def nlp_service(self) -> NLPService:
        return NLPService()

    def test_get_base_words(self, nlp_service):
        words = {'russia', 'thinking', 'bigger', 'big', 'best', 'noun', 'Mary', 'robert', 'talks', 'enough', 'mine',
                 'cats', 'efrvger', 'coming', 'goes'}  # Not found: toward TODO try Spacy NLP service

        base_words = nlp_service.get_base_words(words=words)
        exp_base_words = {'thinking', 'think', 'big', 'best', 'noun', 'talk', 'enough', 'mine', 'cat',
                          'coming', 'come', 'go'}
        assert base_words == exp_base_words, f'Expected words: {exp_base_words}\nreceived words: {base_words}, ' \
                                             f'\ndiff: {exp_base_words.difference(base_words)}'
