import pytest
from src.main.words_parser.text_parser import TextParser


class TestTextParser:

    @pytest.fixture(scope='class')
    def text_parser(self) -> TextParser:
        return TextParser()

    def test_replace_apostrophe_ending(self, text_parser):
        text_with_apostrophe_ending = ["And she can not contact you\nwithout us knowin'.", ]
        exp_words = {'and', 'she', 'can', 'not', 'contact', 'you', 'without', 'us', 'knowing'}
        words = text_parser.text2words(text=text_with_apostrophe_ending)
        assert words == exp_words, f'Expected words: {words}\nreceived words: {words}, ' \
                                   f'\ndiff: {exp_words.difference(words)}'

    def test_apostrophe_start(self, text_parser):
        # apostrophe
        text_with_apostrophe_start = ["- Oh, 'cause you are such a threat.",
                                      "- Get 'em off of us, Mick!"]

        exp_words = {'oh', 'you', 'are', 'such', 'a', 'threat', 'get', 'off', 'of', 'us', 'mick'}
        words = text_parser.text2words(text=text_with_apostrophe_start)
        assert words == exp_words, f'Expected words: {words}\nreceived words: {words}, ' \
                                   f'\ndiff: {exp_words.difference(words)}'

    def test_replace_apostrophe_mid(self, text_parser):
        text_with_apostrophe_mid = ["- Oh, 'cause you're such a threat.",
                                    "don't wouldn't can't."]
        exp_words = {'oh', 'you', 'such', 'a', 'threat', 'do', 'would', 'can'}
        words = text_parser.text2words(text=text_with_apostrophe_mid)
        assert words == exp_words, f'Expected words: {words}\nreceived words: {words}, ' \
                                   f'\ndiff: {exp_words.difference(words)}'

    def test_no_duplicates(self, text_parser):
        text_with_duplicates = ["- Oh, because you are such a threat.",
                                "You do not would not can not."]
        exp_words = {'oh', 'you', 'such', 'a', 'threat', 'do', 'would', 'can', 'not', 'are', 'because'}
        words = text_parser.text2words(text=text_with_duplicates)
        assert words == exp_words, f'Expected words: {words}\nreceived words: {words}, ' \
                                   f'\ndiff: {exp_words.difference(words)}'
