import pytest

from src.main.words_parser.subtitle_parser import SRTSubtitleParser


class TestSRTSubtitleParser:

    @pytest.fixture(scope='class')
    def srt_sub_parser(self) -> SRTSubtitleParser:
        return SRTSubtitleParser()

    def test_subtitles2words(self, srt_sub_parser):
        # 1
        # 00:00:34,534 --> 00:00:35,869
        # Go! Go! Go! Go! Go!
        #
        # 2
        # 00:00:36,369 --> 00:00:38,089
        # - Move it.
        # - Let's get out of here!
        #
        # 3
        # 00:00:50,884 --> 00:00:52,218
        # Headed down Poplar, toward Main.
        test_srt_file_path = '/cinema_flashcards/src/test/test_data/StrangerThingsS02E01.srt'  # TODO
        exp_words = {'go', 'move', 'it', 'let', 'get', 'out', 'of', 'here',
                     'headed', 'down', 'poplar', 'toward', 'main'}
        words = srt_sub_parser.srt_subtitles2words(srt_file_path=test_srt_file_path)
        assert words == exp_words, f'Expected words: {exp_words}\nreceived words: {words}, ' \
                                   f'\ndiff: {exp_words.difference(words)}'
