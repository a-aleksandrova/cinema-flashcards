from src.main.dictionary_service.yandex_dictionary.yandex_dictionary_service import YandexDictionaryService


class TestYandexDictionaryService:
    ya_dict = YandexDictionaryService(lang='en-ru')

    def test_manual_get_words_target_lang_data(self):
        words = ['going', 'turn', 'busy', 'see']  # put any word and check result with debug mode
        words_target_lang_data = self.ya_dict.get_words_target_lang_data(words=words)
        exp_words_data_count = len(words)  # stop debug here
        words_data_count = len(words_target_lang_data)
        assert words_data_count == exp_words_data_count, f'Expected words data count={exp_words_data_count},' \
                                                         f' received: {words_data_count}'

    def test_not_existed_word(self):
        words = ['kdnvkdls']
        words_target_lang_data = self.ya_dict.get_words_target_lang_data(words=words)
        exp_words_data_count = len(words)
        words_data_count = len(words_target_lang_data)
        assert words_data_count == exp_words_data_count, f'Expected words data count={exp_words_data_count},' \
                                                         f' received: {words_data_count}'

        for exp_word, word_data in zip(words, words_target_lang_data):
            word = word_data.word
            assert word == exp_word, f'Expected source word={exp_word}, received: {word}'
            assert not word_data.translations, f'Expected empty translations, received: {word_data.translations}'
            assert not word_data.transcription, f'Expected empty transcription, received: {word_data.transcription}'

    def test_get_words_target_lang_data(self):
        words = ['seal']
        exp_words_translations = {'seal': {'печать', 'тюлень'}}
        words_target_lang_data = self.ya_dict.get_words_target_lang_data(words=words)

        exp_words_data_count = len(words)
        words_data_count = len(words_target_lang_data)
        assert words_data_count == exp_words_data_count, f'Expected words data count={exp_words_data_count},' \
                                                         f' received: {words_data_count}'

        words_translations = {}
        for word_target_lang_data in words_target_lang_data:
            word = word_target_lang_data.word
            translations_data = word_target_lang_data.translations
            word_translations = []
            for part_of_speach_translations in translations_data.values():
                part_of_speach_translations_words = [tr.word for tr in part_of_speach_translations]
                word_translations.extend(part_of_speach_translations_words)

            words_translations[word] = word_translations

        for exp_word in words:
            exp_word_translations = exp_words_translations.get(exp_word)
            word_translations = words_translations.get(exp_word)
            uniq_word_translations = set(word_translations)
            assert len(word_translations) == len(uniq_word_translations), f'Word translations have duplicates, ' \
                                                                          f'word_translations={word_translations}'

            assert exp_word_translations.issubset(uniq_word_translations), f'Received word translations dont contain ' \
                                                                           f'expected translations: ' \
                                                                           f'{exp_word_translations.difference(uniq_word_translations)}'


