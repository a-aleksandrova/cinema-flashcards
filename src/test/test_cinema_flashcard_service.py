import os.path

import pytest

from src.main.anki_api import AnkiConnectAPI, AnkiNote
from src.main.cinema_flashcard.cinema_flashcard_service import CinemaFlashcardService
from src.main.dictionary_service.dictionary_service import DictionaryService
from src.main.dictionary_service.yandex_dictionary.yandex_dictionary_service import YandexDictionaryService
from src.main.flashcard_service.anki.anki_flashcard_service import AnkiFlashcardService
from src.main.nlp_service.nltk_nlp_service import NLPService
from src.main.utils.common import ROOT_PATH
from src.main.words_parser.subtitle_parser import SRTSubtitleParser


class TestCinemaFlashcardService:
    test_film_deck_name = 'Test Film Deck Name for TestCinemaFlashcardService'
    test_lexicon_deck_name = 'Test Lexicon Deck Name for TestCinemaFlashcardService'

    @pytest.fixture(scope='class')
    def cinema_fsh_service(self) -> CinemaFlashcardService:
        bi_ya_dictionary = YandexDictionaryService()
        dictionary = DictionaryService(bilingual_dictionary=bi_ya_dictionary, source_lang_dictionary=None)

        flashcard_service = AnkiFlashcardService(film_deck_name=self.test_film_deck_name,
                                                 lexicon_deck_name=self.test_lexicon_deck_name)
        subs_parser = SRTSubtitleParser()
        nlp_service = NLPService()
        cinema_fsh_service = CinemaFlashcardService(flashcard_service=flashcard_service,
                                                    dictionary=dictionary,
                                                    subs_parser=subs_parser,
                                                    nlp_service=nlp_service)

        return cinema_fsh_service

    @pytest.fixture(scope='class')
    def anki_api(self) -> AnkiConnectAPI:
        anki_api = AnkiConnectAPI()
        return anki_api

    # def teardown_method(self, anki_api):
    #     # Delete test decs
    #     anki_api.delete_decks(decks_names=[self.test_film_deck_name, self.test_lexicon_deck_name])

    def test_create_film_flashcards(self, cinema_fsh_service, anki_api):
        lexicon_notes_ids_before = set(anki_api.find_notes(query=f'deck:"{self.test_lexicon_deck_name}"'))
        lexicon_notes_before = anki_api.notes_info(ids=lexicon_notes_ids_before)

        test_srt_subs_file_path = os.path.join(ROOT_PATH, "src/test/test_data/StrangerThingsS02E01.srt")

        # Action 1: Create film deck
        cinema_fsh_service.create_film_flashcards(srt_subs_file_path=test_srt_subs_file_path)

        # Check 1: decks created
        decks = anki_api.get_deck_names_and_ids()
        lexicon_deck = decks.get(self.test_lexicon_deck_name)
        assert lexicon_deck, f'Lexicon deck ({self.test_lexicon_deck_name}) not created.'
        film_deck = decks.get(self.test_film_deck_name)
        assert film_deck, f'Film deck ({self.test_film_deck_name}) not created.'

        # Check 2: words in film deck
        film_notes_ids = set(anki_api.find_notes(query=f'deck:"{self.test_film_deck_name}"'))
        film_notes = anki_api.notes_info(ids=film_notes_ids)
        film_notes_by_word = dict([(note.fields.front.value, note) for note in film_notes])
        assert film_notes_ids, 'Film deck dont contain notes.'
        # TODO manual check film deck flashcards in Anki | lexicon notes moved to film deck | new notes created

        # Check lexicon notes in film deck not changed
        lexicon_notes_by_word_before = dict([(note.fields.front.value, note) for note in lexicon_notes_before])

        for film_word in film_notes_by_word:
            film_note = film_notes_by_word.get(film_word)
            lexicon_note = lexicon_notes_by_word_before.get(film_word)
            if lexicon_note:  # else its new word
                compare_cards(exp_note=lexicon_note, note=film_note)
            # TODO check card progress saved

        # Action 2: Merge film deck to lexicon deck
        cinema_fsh_service.merge_film_deck_to_lexicon()

        # 3. Check film deck deleted
        decks = anki_api.get_deck_names_and_ids()
        film_deck_id = decks.get(self.test_film_deck_name)
        assert not film_deck_id, f'Film deck ({self.test_film_deck_name}) not deleted, id={film_deck_id}.'

        # 4. Check non film notes ids not changed
        lexicon_notes_ids_after_merge = set(anki_api.find_notes(query=f'deck:"{self.test_lexicon_deck_name}"'))
        lexicon_non_film_notes_ids = lexicon_notes_ids_after_merge.difference(film_notes_ids)
        lexicon_non_film_notes_ids_before = lexicon_notes_ids_before.difference(film_notes_ids)
        assert lexicon_non_film_notes_ids == lexicon_non_film_notes_ids_before, f'Old lexicon notes are not equal notes ' \
                                                                       f'before film deck merge, diff: ' \
                                                                       f'Removed: {lexicon_notes_ids_before.difference(lexicon_non_film_notes_ids)}' \
                                                                       f'\nUnexpected: {lexicon_non_film_notes_ids.difference(lexicon_notes_ids_before)}'

        # 5. Check film deck words moved to lexicon deck
        lexicon_film_notes_ids = lexicon_notes_ids_after_merge.difference(lexicon_non_film_notes_ids)
        assert film_notes_ids == lexicon_film_notes_ids, f'Film notes not moved to lexicon: ' \
                                                         f'{film_notes_ids.difference(lexicon_film_notes_ids)}'
        # 6. Check film notes not changed
        lexicon_film_notes = anki_api.notes_info(ids=film_notes_ids)
        lexicon_film_notes_by_word = dict([(note.fields.front.value, note) for note in lexicon_film_notes])

        for film_word in film_notes_by_word:
            film_note = film_notes_by_word.get(film_word)
            lexicon_note = lexicon_film_notes_by_word.get(film_word)
            assert lexicon_note, f'Film note for word={film_word} not moved to lexicon. Note: {film_note}'
            compare_cards(exp_note=film_note, note=lexicon_note)
            # TODO check card progress saved

        # 7. Check non film notes not changed
        lexicon_non_film_notes = anki_api.notes_info(ids=lexicon_non_film_notes_ids)
        lexicon_non_film_notes_by_word = dict([(note.fields.front.value, note) for note in lexicon_non_film_notes])
        for lexicon_word in lexicon_non_film_notes_by_word:
            lexicon_note_before = lexicon_notes_by_word_before.get(lexicon_word)
            lexicon_note = lexicon_non_film_notes_by_word.get(lexicon_word)
            assert lexicon_note, f'Film note for word={lexicon_word} not exist in lexicon. Note: {lexicon_note_before}'
            compare_cards(exp_note=lexicon_note_before, note=lexicon_note)


def compare_cards(exp_note: AnkiNote, note: AnkiNote) -> None:
    assert note.fields.front == exp_note.fields.front, f'Expected note front={exp_note.fields.front}, ' \
                                                       f'received: {note.fields.front}'
    assert note.fields.back == exp_note.fields.back, f'Expected note back={exp_note.fields.back}, ' \
                                                     f'received: {note.fields.back}'
    assert note.cards == exp_note.cards, f'Expected cards_ids: {exp_note.cards}, \nreceived: {note.cards}.'
