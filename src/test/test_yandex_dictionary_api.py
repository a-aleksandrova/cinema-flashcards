import logging

from src.main.dictionary_service.yandex_dictionary.yandex_dictionary_api import YandexDictionaryAPI


class TestYandexDictionaryAPI:
    ya_api = YandexDictionaryAPI()

    def test_lookup(self):
        logging.basicConfig(level=logging.DEBUG)
        en_word = 'seal'
        exp_ru_word = ['печать', 'тюлень']
        lookup_result = self.ya_api.lookup(lang='en-ru', text=en_word)
        word_translations = lookup_result.def_
        word_translations_count = len(word_translations)
        assert word_translations_count > 1, f'Word translation not received, word_translations={word_translations}'

        first_ru_definition = word_translations[0]
        first_ru_translation = first_ru_definition.tr[0]
        ru_word = first_ru_translation.text
        assert ru_word in exp_ru_word, f'Expected ru words: {en_word}; received {ru_word}'

