import re

import nltk
from nltk import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.corpus.reader import Synset

from src.main.utils.logger_cfg import LoggingHandler


# class PartOfSpeech(Enum):
#     noun = 'noun'
#     verb = 'verb'
#     adjective = 'adjective'
#     adverbs = 'adverbs'
#     satellite_adjectives = 'satellite-adjectives'


class NLPService(LoggingHandler):  # TODO refactoring

    def __init__(self):
        super().__init__()
        nltk.download("wordnet")  # TODO refactoring?
        nltk.download("omw-1.4")  # TODO refactoring?
        self.wnl = WordNetLemmatizer()  # TODO refactoring?

    def get_base_words(self, words: set[str], lowercase: bool = True) -> set[str]:
        """
        Get base forms of words.
        1. Remove Proper Names: location and person.
        2. Remove words without Synsets. Synset -- set of different senses of a particular word.
        3. Return only base words (lemma). Ex: thinking -> thinking (base noun), think (base)

        :param words: Set of Words.
        :param lowercase: Lowercase all words.
        :return: Set of unique base forms of Words.
        """
        if lowercase:
            words = {word.lower() for word in words}  # TODO refactoring?

        valid_words_1 = self.remove_proper_names(words=words)
        valid_words_2 = self.get_words_lemmas(words=valid_words_1)

        return valid_words_2

    def get_words_lemmas(self, words: set[str]) -> set[str]:
        """
        Get only base words (lemma).
        Ex: thinking -> thinking (base noun), think (base verb)
        :param words: Set of Words.
        :return: Set of unique words lemmas.
        """
        words_sunsets = self.get_words_with_synsets(words=words)
        all_lemmas = set()
        for word in words_sunsets:
            word_sunsets = words_sunsets[word]
            word_lemmas = self._get_word_lemmas(word=word, synsets=word_sunsets)
            all_lemmas.update(word_lemmas)

        return all_lemmas

    def remove_proper_names(self, words: set[str]) -> set[str]:
        """
        Remove proper names words: location and person.
        :param words: Set of words.
        :return: Set of words without proper names.
        """

        proper_name_words = set()
        for word in words:
            is_proper_name = self._is_proper_name(word=word)
            if is_proper_name:
                proper_name_words.add(word)

        if proper_name_words:
            self.logger.info(msg=f'Removed proper name words: {proper_name_words}')

        valid_words = words.difference(proper_name_words)

        return valid_words

    def get_words_with_synsets(self, words: set[str]) -> dict[str, list[Synset]]:
        """
        Get NLTK Synsets for words.
        Synset -- set of different senses of a particular word.

        Return only words for which Synsets are found.
        :param words: Set of unique words.
        :return: Words with its synsets as dict.
        """
        words_synsets = {}
        not_found_words = set()
        for word in words:
            word_synsets = wordnet.synsets(word)
            if word_synsets:
                words_synsets[word] = word_synsets
            else:
                not_found_words.add(word)

        if not_found_words:
            self.logger.debug(msg=f'Synsets not found for words: {not_found_words}')

        return words_synsets

    def _get_word_lemmas(self, word, synsets: list[Synset]) -> set[str]:
        """
        Get unique word lemmas.
        :param word: Word.
        :param synsets: Word NLKT synsets.
        :return: Set of unique lemmas.
        """
        synsets_per_pos = self._group_synsets_by_pos(synsets=synsets)

        word_lemmas = set()
        for pos in synsets_per_pos:  # Get word lemmas for each part of speech
            pos_synsets = synsets_per_pos[pos]
            if pos_synsets:
                word_lemma = self.wnl.lemmatize(word, pos=pos)
                word_lemmas.add(word_lemma)

        return word_lemmas

    @staticmethod
    def _group_synsets_by_pos(synsets: list[Synset]) -> dict[str, list[Synset]]:
        """
        Groups synsets by its parts of speech.
        :param synsets: NLTK synsets.
        :return: Parts of speech and their synsets as dict.
        """
        synsets_by_pos = {}
        for synset in synsets:
            pos = synset.pos()
            pos_synset_data = synsets_by_pos.get(pos) or []
            pos_synset_data.append(synset)
            synsets_by_pos[pos] = pos_synset_data

        return synsets_by_pos

    def _is_proper_name(self, word: str) -> bool:
        """
        Checks the direct meaning of the word is a proper name.
        Proper names types:  person, location.
        :param word: Word.
        :return: True if word is a proper name else False.
        """
        proper_lexnames = ['noun.location', 'noun.person']

        is_proper_name = False
        noun_synsets = wordnet.synsets(word, pos='n')
        if noun_synsets:
            first_synset = noun_synsets[0]
            if first_synset.lexname() in proper_lexnames:  # if direct meaning in proper lexnames
                word_lemmas = first_synset.lemma_names()
                if not word_lemmas:  # word don't have lemma
                    self.logger.warning(msg=f'Word "{word}" dont have lemma, first synset: {first_synset}')
                    return True
                first_lemmas = word_lemmas[:2] if len(word_lemmas) > 1 else word_lemmas[:1]
                cap_word = word[0].upper() + word[1:]  # word starts with capital letter
                is_proper_name = cap_word in first_lemmas

        return is_proper_name

    @staticmethod
    def _nltk_synset_name2data(name: str) -> (str, str, int):
        """
        Return lemma, part of speech and senses number from synset name.
        Expected name format: <lemma>.<pos>.<num>
        Ex: approach.n.02
        :param name: NLTK Synset name.
        :return: Lemma, part of speech, senses number.
        """
        name_pos_num_pattern = r"([\w.]+)\.(\w+)\.(\d+)"  # approach.n.02, o.k..n.01
        matches = re.findall(pattern=name_pos_num_pattern, string=name)
        if len(matches) != 1:
            raise ValueError(f'[Unexpected synset name format: {name}. '
                             f'Expected format: <lemma>.<pos>.<num> '
                             f'Ex: approach.n.02')
        lemma, pos, num = matches[0]
        return lemma, pos, int(num)
