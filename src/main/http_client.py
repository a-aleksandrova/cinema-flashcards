from requests import Session, Response


class HTTPClient:
    def __init__(self, *, url: str):
        self.session = Session()
        self.url = url

    def get(self, *,
            uri: str | None = None,
            params: dict | None = None,
            payload: dict = None,
            headers: dict | None = None) -> Response:
        return self._send(method='GET', uri=uri, params=params, payload=payload, headers=headers)

    def post(self, *,
             uri: str | None = None,
             params: dict | None = None,
             payload: dict | None = None,
             headers: dict | None = None) -> Response:
        return self._send(method='POST', uri=uri, params=params, payload=payload, headers=headers)

    def _send(self, *,
              method: str,
              uri: str | None = None,
              params: dict | None = None,
              payload: dict | None = None,
              headers: dict | None = None) -> Response:
        url = f"{self.url}{uri or ''}"
        return self.session.request(
            method=method,
            url=url,
            params=params,
            json=payload,
            headers=headers
        )

    def __exit__(self) -> None:
        self.session.close()
