from src.main.dictionary_service.dictionary_service import WordMeaning
from src.main.flashcard_service.models import NewFlashCard
from src.main.utils.logger_cfg import LoggingHandler


class FlashcardService(LoggingHandler):
    _lexicon_deck_name: str

    def get_non_lexicon_words(self, words: set[str]) -> set[str]:
        """
        Get words that the lexicon deck doesn't contain.
        :param words: Words that need a check to enter the lexicon.
        :return: Words that the lexicon doesn't contain.
        """
        pass

    def add_new_lexicon_flashcards(self, flashcards: list[NewFlashCard]) -> None:
        """
        Add new flashcards to lexicon deck.
        :param flashcards: New flashcards.
        """
        pass

    def move_lexicon_words_to_film_deck(self, words: set[str]) -> None:
        """
        Find lexicon flashcards for specified words and move it to film deck.
        :param words: Words.
        """
        pass

    def merge_film_deck_to_lexicon(self) -> None:
        """
        Move all film deck flashcards to lexicon deck.
        Remove empty film deck.
        TODO если в лексиконе появились дубликаты карт из колоды кино?
        """
        pass

    def words_meaning2new_flashcards(self, words_data: list[WordMeaning]) -> list[NewFlashCard]:
        pass

