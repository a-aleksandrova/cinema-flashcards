class Deck:
    def __init__(self, name: str, id_: int):
        self.name = name
        self.id_ = id_


class FlashCard:
    def __init__(self, *,  word: str, meaning: str, language: str = 'en-ru'):  # TODO: move language to cfg?
        self.word = word
        self.meaning = meaning  # TODO use WordMeaning?
        self.language = language


class ExistedFlashCard(FlashCard):
    def __init__(self, *, word: str, meaning: str, card_id: int, sub_cards_ids: list[int],
                 language: str = 'en-ru'):
        super().__init__(word=word, meaning=meaning, language=language)
        self.card_id = card_id
        self.sub_cards_ids = sub_cards_ids


class NewFlashCard(FlashCard):
    def __init__(self, *, word: str, meaning: str, language: str = 'en-ru'):
        super().__init__(word=word, meaning=meaning, language=language)
