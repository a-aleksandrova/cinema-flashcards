from src.main.flashcard_service.models import Deck


class AnkiDeck(Deck):
    def __init__(self, name: str, id_: int):
        super().__init__(name=name, id_=id_)
