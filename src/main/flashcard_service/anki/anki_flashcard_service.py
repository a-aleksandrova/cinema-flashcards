from src.main.dictionary_service.dictionary_service import WordMeaning
from src.main.flashcard_service.anki.anki_connect import AnkiConnectService, AnkiDeck
from src.main.flashcard_service.flashcard_sevice import FlashcardService
from src.main.flashcard_service.models import ExistedFlashCard, NewFlashCard


class AnkiFlashcardService(FlashcardService):
    anki_connect = AnkiConnectService()

    def __init__(self, film_deck_name: str, lexicon_deck_name: str = 'Cinema Flashcard Lexicon'):
        super().__init__()
        self._lexicon_deck_name = lexicon_deck_name
        self._film_deck_name = film_deck_name
        self.lexicon_deck: AnkiDeck = self._get_lexicon_deck() or self._create_lexicon_deck()  # TODO refactoring
        self.film_deck = self._create_film_deck()  # TODO refactoring

    def get_non_lexicon_words(self, words: set[str]) -> set[str]:
        """
        Get words that the lexicon deck doesn't contain.
        :param words: Words that need a check to enter the lexicon.
        :return: Words that the lexicon doesn't contain.
        """
        lexicon_words = self._get_lexicon_words()
        non_lexicon_words = words.difference(lexicon_words)

        return non_lexicon_words

    def add_new_lexicon_flashcards(self, flashcards: list[NewFlashCard]) -> None:
        """
        Add new flashcards to lexicon deck.
        :param flashcards: New flashcards.
        """
        self.anki_connect.add_new_flashcards_to_deck(flashcards=flashcards, deck=self.lexicon_deck, check_notes=True)

    def move_lexicon_words_to_film_deck(self, words: set[str]) -> None:
        """
        Find flashcards for specified words and move its to film deck.
        :param words: Words.
        """
        # Check film deck is empty
        deck_is_empty = self.anki_connect.deck_is_empty(deck=self.film_deck)
        if not deck_is_empty:
            error = f'Impossible move flashcards to not empty film deck ' \
                    f'Deck name={self.film_deck.name}, id={self.film_deck.id_}'
            self.logger.critical(msg=error)
            raise Exception(error)  # TODO refactoring
        # Move words
        lexicon_notes = self.anki_connect.get_deck_notes_by_words(words=words, deck=self.lexicon_deck)
        self.anki_connect.move_notes_to_deck(notes=lexicon_notes, deck=self.film_deck)
        self.logger.info(msg=f'Words from lexicon deck ({self.lexicon_deck.name}) moved to '
                             f'film deck ({self.film_deck.name}): {len(lexicon_notes)}')

    def merge_film_deck_to_lexicon(self) -> None:
        """
        Move all film deck flashcards to lexicon deck.
        Remove empty film deck.
        """
        film_notes = self.anki_connect.get_deck_notes(deck=self.film_deck)
        self.anki_connect.move_notes_to_deck(notes=film_notes, deck=self.lexicon_deck)
        self.logger.debug(msg=f'Words from film deck ({self.film_deck.name}) moved to '
                              f'lexicon deck ({self.lexicon_deck.name}): {len(film_notes)}')
        self._delete_empty_film_deck()

    def words_meaning2new_flashcards(self, words_data: list[WordMeaning]) -> list[NewFlashCard]:
        new_flashcards = [self._word_meaning2new_flashcard(word_data=word_data) for word_data in words_data]
        return new_flashcards

    @staticmethod
    def _word_meaning2new_flashcard(word_data: WordMeaning) -> NewFlashCard:  # TODO it shouldn't be here, but where?

        # 1. Target lang meaning

        # Create main translation block
        main_translation = word_data.target_lang_meaning.main_translation.word
        main_translation_block = f'<b>{main_translation}</b><br><br>'

        # Create parts of speech translations block
        translations = word_data.target_lang_meaning.translations
        translations_strs = []
        for translation in translations:
            tr_word = translation.word
            part_of_speech = translation.part_of_speech
            translations_str = f'{tr_word}<sup>[{part_of_speech}]</sup>'
            translations_strs.append(translations_str)
        translations_block = ', '.join(translations_strs)

        # 2. TODO Source lang meaning, audio, picture and etc

        meaning = main_translation_block + translations_block

        flashcard = NewFlashCard(word=word_data.word,
                                 meaning=meaning,
                                 language=word_data.lang)

        return flashcard

    def _get_lexicon_deck(self) -> AnkiDeck:
        """
        Get existed lexicon deck or None if deck is not exist.
        :return: Lexicon deck.
        """
        lexicon_deck_id = self.anki_connect.get_deck_id(deck_name=self._lexicon_deck_name)
        if lexicon_deck_id:
            lexicon_deck = AnkiDeck(id_=lexicon_deck_id, name=self._lexicon_deck_name)
        else:
            self.logger.info(msg=f'Lexicon Deck with name={self._lexicon_deck_name} not exists.')
            lexicon_deck = None

        return lexicon_deck

    def _create_lexicon_deck(self) -> AnkiDeck:
        """
        Create empty Lexicon deck.
        Rise error if Lexicon deck already exist.
        :return: Lexicon deck ID.
        """
        lexicon_deck = self.anki_connect.create_new_deck(name=self._lexicon_deck_name)
        self.logger.info(msg=f'Created new Lexicon Deck with name="{lexicon_deck.name}", id={lexicon_deck.id_}.')
        return lexicon_deck

    def _create_film_deck(self) -> AnkiDeck:
        """
        Create empty Lexicon deck.
        Rise error if Lexicon deck already exist.
        :return: Lexicon deck ID.
        """
        if self._film_deck_name == self.lexicon_deck.name:
            self.logger.error(msg=f'Film deck name cant be equal lexicon deck name: {self._lexicon_deck_name}')
            raise Exception(f'Film deck name cant be equal lexicon deck name: {self._lexicon_deck_name}')

        film_deck = self.anki_connect.create_new_deck(name=self._film_deck_name)
        self.logger.info(msg=f'Created new Film Deck with name="{film_deck.name}", id={film_deck.id_}.')
        return film_deck

    def _get_lexicon_words(self) -> set[str]:
        """
        Get all words from lexicon deck.
        :return: Lexicon words set.
        """
        return self._get_deck_words(deck=self.lexicon_deck)

    def _get_deck_words(self, deck: AnkiDeck) -> set[str]:
        """
        Get all words in deck.
        :param deck: Deck.
        :return: Words set.
        """
        notes = self.anki_connect.get_deck_notes(deck=deck)
        words = [note.fields.front.value for note in notes]
        words_set = set(words)

        # check no duplicates TODO separate
        len_words = len(words)
        len_words_set = len(words_set)
        if len_words_set < len_words:
            self.logger.warning(msg=f'Deck (name={deck.name}, id={deck.id_}) contain duplicate words, '
                                    f'all words: {words}')

        return words_set

    def _get_words_flashcards(self, words: set[str], deck: AnkiDeck) -> list[ExistedFlashCard]:
        """
        Get flashcards for specified words from specified deck.
        :param words: Words.
        :param deck: Anki deck.
        :return: Words flashcards.
        """
        notes = self.anki_connect.get_deck_notes_by_words(words=words, deck=deck)
        flashcards = self.anki_connect.notes2flashcards(notes=notes)

        return flashcards

    def _delete_empty_film_deck(self):
        """
        Delete empty film deck. Raise exceptions if deck not empty.
        """
        self.anki_connect.delete_empty_deck(deck=self.film_deck)
        self.logger.debug(f'Film deck deleted: {self.film_deck.name, self.film_deck.id_}')
