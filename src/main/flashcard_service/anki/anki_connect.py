from src.main.anki_api import AnkiConnectAPI, AnkiNote, AnkiNewNote, NewNoteFields, NewNoteOptions, \
    DuplicateScopeOptions
from src.main.flashcard_service.anki.models import AnkiDeck
from src.main.flashcard_service.models import NewFlashCard, ExistedFlashCard
from src.main.utils.logger_cfg import LoggingHandler


class AnkiConnectService(LoggingHandler):

    def __init__(self):
        super().__init__()
        self.api = AnkiConnectAPI()
        self.new_notes_options = self._no_any_duplicate_options()

    def get_deck_notes(self, deck: AnkiDeck) -> list[AnkiNote]:
        """
        Get all notes for deck with specified name.
        :param deck: Anki Deck.
        :return: Notes.
        """
        notes_ids = self.api.find_notes(query=f'deck:"{deck.name}"')
        notes = self.api.notes_info(ids=notes_ids)
        return notes

    def get_deck_notes_by_words(self, words: set[str], deck: AnkiDeck) -> list[AnkiNote]:
        """
        Get notes for specified words from deck with specified name.
        :param words: Words.
        :param deck: Anki Deck.
        :return: Notes.
        """
        all_notes = self.get_deck_notes(deck=deck)
        words2notes = dict([(note.fields.front.value, note) for note in all_notes])

        not_exist_words = set()
        words_notes = []
        for word in words:
            word_note = words2notes.get(word)
            if word_note:
                words_notes.append(word_note)
            else:
                not_exist_words.add(word)

        # warning if deck don't contain some words
        if not_exist_words:
            self.logger.warning(msg=f'Anki Deck (id={deck.name}, name={deck.name}) dont contain notes for '
                                    f'words: {not_exist_words}')
        return words_notes

    def _move_cards_to_deck(self, cards_ids: list[int], deck: AnkiDeck) -> None:
        """
        Move cards with specified IDs to spesified deck.
        :param cards_ids: Cards IDs.
        :param deck: Deck.
        """
        self.api.change_deck(cards=cards_ids, deck_name=deck.name)

    def create_new_deck(self, name: str) -> AnkiDeck:
        self._check_deck_not_exist(deck_name=name)
        deck_id = self.api.create_deck(deck_name=name)
        return AnkiDeck(name=name, id_=deck_id)

    def _check_deck_not_exist(self, deck_name: str):
        deck_id = self.get_deck_id(deck_name=deck_name)
        if deck_id:
            raise Exception(f'Deck with name={deck_name} already exists by id={deck_id}.')

    def get_deck_id(self, deck_name: str) -> int | None:
        """
        Get deck ID by deck name.
        :param deck_name:
        :return: Deck ID or None if deck not exists.
        """
        decks = self.api.get_deck_names_and_ids()
        deck_id = decks.get(deck_name)
        return deck_id

    def add_new_flashcards_to_deck(self, flashcards: list[NewFlashCard], deck: AnkiDeck,
                                   check_notes: bool = False) -> list[int]:
        """
        Add new flashcards to specified deck.
        :param check_notes:
        :param flashcards: Flashcards to add.
        :param deck: Anki deck.
        :return: Added word notes IDs.
        """

        # convert flashcards to new anki notes
        notes = self.new_flashcards2new_notes(flashcards=flashcards, deck=deck)

        if check_notes:
            # get can be added notes
            success_notes = self._can_add_notes(notes=notes, deck=deck)
        # try to add all words [anki notes] TODO: add success notes (cards) only???
        added_notes_ids = self.api.add_notes(notes=notes)
        # TODO check all notes added?

        return added_notes_ids

    def move_notes_to_deck(self, notes: list[AnkiNote], deck: AnkiDeck) -> None:
        """
        Move notes (all notes cards) to another deck.
        :param notes: Notes to move.
        :param deck: Deck.
        """
        notes_cards_ids = []
        for note in notes:
            cards_ids = note.cards
            notes_cards_ids.extend(cards_ids)

        self._move_cards_to_deck(cards_ids=notes_cards_ids, deck=deck)

    def deck_is_empty(self, deck: AnkiDeck) -> bool:
        """
        Checks the deck contains notes. Deck is empty if deck not contains notes.

        :param deck: Anki deck.
        :return: True if deck is empty else False.
        """
        deck_notes = self.get_deck_notes(deck=deck)
        is_empty = not deck_notes
        return is_empty

    def delete_empty_deck(self, deck: AnkiDeck):
        # Check deck empty
        deck_is_empty = self.deck_is_empty(deck=deck)
        if not deck_is_empty:
            raise Exception(f'Impossible to delete deck that contains notes: deck name={deck.name}, id={deck.id_}')
        # Delete deck
        self.api.delete_decks(decks_names=[deck.name], cards_too=True)
        # Error from AnkiConnect: Since Anki 2.1.28 it's not possible to delete decks without deleting cards as well

        # Check deck deleted
        deck_id = self.get_deck_id(deck_name=deck.name)
        if deck_id:
            self.logger.error(msg=f'Failed to remove deck with name={deck.name}. '
                              f'Deck founded by id={deck_id}. Investigate.')

    def new_flashcard2new_note(self, flashcard: NewFlashCard, deck: AnkiDeck) -> AnkiNewNote:
        new_note = AnkiNewNote(deck_name=deck.name,
                               fields=NewNoteFields(front=flashcard.word,
                                                    back=flashcard.meaning),
                               options=self.new_notes_options,
                               audio=[],
                               video=[],
                               picture=[],
                               tags=[flashcard.language])
        return new_note

    def new_flashcards2new_notes(self, flashcards: list[NewFlashCard], deck: AnkiDeck) -> list[AnkiNewNote]:
        return [self.new_flashcard2new_note(flashcard=flashcard, deck=deck) for flashcard in flashcards]

    @staticmethod
    def note2flashcard(note: AnkiNote) -> ExistedFlashCard:
        word = ExistedFlashCard(word=note.fields.front.value,
                                meaning=note.fields.back.value,
                                card_id=note.note_id,
                                sub_cards_ids=note.cards)
        return word

    def notes2flashcards(self, notes: list[AnkiNote]) -> list[ExistedFlashCard]:
        return [self.note2flashcard(note=note) for note in notes]

    @staticmethod
    def _no_any_duplicate_options() -> NewNoteOptions:
        options = NewNoteOptions(allow_duplicate=False,
                                 duplicate_scope='deck',
                                 duplicate_scope_options=DuplicateScopeOptions(deck_name='Default',
                                                                               check_children=False,
                                                                               check_all_models=False))
        return options

    def _can_add_notes(self, notes: list[AnkiNewNote], deck: AnkiDeck) -> list[AnkiNewNote]:
        """
        Check can add notes [cards] to deck.
        :param notes:
        :param deck:
        :return:
        """
        result = self.api.can_add_notes(notes=notes)

        # get failed and success notes
        success_notes, failed_notes = [], []
        for note_idx, can_be_added in enumerate(result):
            note = notes[note_idx]
            if can_be_added:
                success_notes.append(note)
            else:
                failed_notes.append(note)

        if failed_notes:
            # check deck already contain notes [cards]
            # all_words = set([note.fields.front for note in notes])  # TODO: check no duplicates
            failed_words = set([note.fields.front for note in failed_notes])  # TODO: check no duplicates
            existed_failed_notes = self.get_deck_notes_by_words(words=failed_words, deck=deck)
            existed_words = set([note.fields.front for note in existed_failed_notes])  # TODO: check no duplicates

            if existed_words:  # deck already contain some or all failed notes
                self.logger.warning(msg=f'Some cards cant be added in deck={deck}, because cards for these words '
                                    f'already exist: {existed_words}.')

            if len(failed_words) > len(existed_words):  # deck not contain some failed notes [words]
                not_existed_words = failed_words.difference(existed_words)
                not_existed_notes = [note for note in failed_notes if note.fields.front not in existed_words]
                self.logger.error(msg=f'Some notes cant be added in deck={deck} and deck not contain these words: '
                                  f'{not_existed_words}\nInvestigate for these notes: {not_existed_notes}.')
        return success_notes
