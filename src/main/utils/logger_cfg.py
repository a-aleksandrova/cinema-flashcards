import os
from logging import Logger

from src.main.utils.common import ROOT_PATH
import logging.config

DEBUG = True
LOG_FILENAME = os.path.join(ROOT_PATH, '.log_file.log')

LOGGING_CONF = {
    "disable_existing_loggers": False,
    "version": 1,
    "formatters": {
        "default": {
            "format": "%(asctime)s [%(process)d:%(lineno)d] %(name)s [%(levelname)s] %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "simple": {
            "format": "%(message)s",
        },
        # "json": {  # The formatter name
        #     "()": "pythonjsonlogger.jsonlogger.JsonFormatter",  # The class to instantiate!
        #     # Json is more complex, but easier to read, display all attributes!
        #     "format": """
        #             asctime: %(asctime)s
        #             created: %(created)f
        #             filename: %(filename)s
        #             funcName: %(funcName)s
        #             levelname: %(levelname)s
        #             levelno: %(levelno)s
        #             lineno: %(lineno)d
        #             message: %(message)s
        #             module: %(module)s
        #             msec: %(msecs)d
        #             name: %(name)s
        #             pathname: %(pathname)s
        #             process: %(process)d
        #             processName: %(processName)s
        #             relativeCreated: %(relativeCreated)d
        #             thread: %(thread)d
        #             threadName: %(threadName)s
        #             exc_info: %(exc_info)s
        #         """,
        #     "datefmt": "%Y-%m-%d %H:%M:%S",  # How to display dates
        # },
    },
    "handlers": {
        "logfile": {  # The handler name
            "formatter": "default",  # Refer to the formatter defined above
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",  # OUTPUT: Which class to use
            "filename": LOG_FILENAME,  # Param for class above. Defines filename to use, load it from constant
            "backupCount": 10,  # Param for class above. Defines how many log files to keep as it grows
        },
        "console": {  # The handler name
            "formatter": "default",  # Refer to the formatter defined above
            "level": "DEBUG",  # FILTER: All logs
            "class": "logging.StreamHandler",  # OUTPUT: Which class to use
            "stream": "ext://sys.stdout",  # Param for class above. It means stream to console
        },
        "verbose_output": {  # The handler name
            "formatter": "simple",  # Refer to the formatter defined above
            "level": "INFO",
            "class": "logging.StreamHandler",  # OUTPUT: Which class to use
            "stream": "ext://sys.stdout",  # Param for class above. It means stream to console
        },
        # "json": {  # The handler name
        #     "formatter": "json",  # Refer to the formatter defined above
        #     "class": "logging.StreamHandler",  # OUTPUT: Same as above, stream to console
        #     "stream": "ext://sys.stdout",
        # },
    },
    "loggers": {
        "user": {  # The name of the logger, this SHOULD match your module!
            "level": "INFO",  # FILTER: only INFO logs onwards from "tryceratops" logger
            "handlers": [
                "verbose_output",  # Refer the handler defined above
            ],
        },
        "main": {
            "level": "DEBUG",
            "handlers": [
                "console",
                "logfile",  # Refer the handler defined above
            ],
        }
    },
    "root": {  # All loggers (including main, test)
        "level": "NOTSET",  # FILTER: only INFO logs onwards
        "handlers": [],
    }
}

logging.config.dictConfig(LOGGING_CONF)
logger = logging.getLogger('main')
logger.info("loggers configured: %s ", ", ".join(LOGGING_CONF["loggers"]))


class LoggingHandler:
    def __init__(self):
        self.logger = logging.getLogger(f'main.{self.__module__} [{self.__class__.__name__}]')
