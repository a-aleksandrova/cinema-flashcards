import logging
from dataclasses import dataclass
from typing import Literal
from src.main.http_client import HTTPClient

from adaptix import Retort, name_mapping, NameStyle

from src.main.utils.logger_cfg import LoggingHandler


@dataclass
class NewNoteFields:
    front: str
    """Front card content"""
    back: str
    """Back card content"""


@dataclass
class DuplicateScopeOptions:
    deck_name: str
    check_children: bool = False
    check_all_models: bool = False


@dataclass
class NewNoteOptions:
    """New Anki note create options"""
    duplicate_scope: Literal['deck']
    duplicate_scope_options: DuplicateScopeOptions
    allow_duplicate: bool = False
    # ...


@dataclass
class AnkiNewNote:
    """Represents Anki Note."""
    # cinema_flashcards
    deck_name: str
    """Deck name."""

    fields: NewNoteFields
    """Card content."""
    options: NewNoteOptions
    """Note options."""
    audio: list
    """Note audios"""
    tags: list
    video: list
    """Note videos."""
    picture: list
    """Note pictures."""
    model_name: str = 'Basic'
    """Note model, required."""


@dataclass
class FieldData:
    value: str
    order: int


@dataclass
class NoteFields:
    front: FieldData
    back: FieldData


@dataclass
class AnkiNote:
    fields: NoteFields
    note_id: int
    cards: list[int]
    model_name: str
    tags: list[str] = NoteFields


class AnkiConnectAPI(LoggingHandler):
    """
    API: https://git.foosoft.net/alex/anki-connect
    Wiki: https://docs.ankiweb.net/#/
    """

    def __init__(self):
        """
        version -> When the provided version is level 4 or below, the API response will only contain the value of
        the result; no error field is available for error handling.

        json_converter -> Retort
        https://adaptix.readthedocs.io/en/latest/index.html
        alias:
        1. underscore: https://adaptix.readthedocs.io/en/latest/loading-and-dumping/extended-usage.html#stripping-underscore
        2. name-style: https://adaptix.readthedocs.io/en/latest/loading-and-dumping/extended-usage.html#name-style

        """
        super().__init__()
        self.url: str = 'http://127.0.0.1:8765'  # TODO move to cfg
        self.client: HTTPClient = HTTPClient(url=self.url)
        self.version: int = 6
        self.json_converter = Retort(
            recipe=[name_mapping(NoteFields, name_style=NameStyle.PASCAL),
                    name_mapping(name_style=NameStyle.CAMEL)]
        )

    def _rpc_request(self, action: str, params: dict = None):
        payload = {
            "action": action,
            "version": self.version,
        }
        if params:
            payload["params"] = params

        resp = self.client.post(payload=payload)
        response = resp.json()

        # check response
        if len(response) != 2:
            error = 'Response has an unexpected number of fields'
            self.logger.error(msg=error)
            raise Exception(error)
        if 'error' not in response:
            error = 'Response is missing required "error" field'
            self.logger.error(msg=error)
            raise Exception()
        if 'result' not in response:
            error = 'Response is missing required "result" field'
            self.logger.error(msg=error)
            raise Exception(error)
        if response['error'] is not None:
            error = f'Error from AnkiConnect: {response["error"]}'
            self.logger.error(msg=error)
            raise Exception(error)

        return response['result']

    def create_deck(self, deck_name: str) -> int:
        """
        Create a new empty deck. Will not overwrite a deck that exists with the same name.
        :param deck_name: Deck name.
        :return: Deck ID.
        """
        params = {'deck': deck_name}
        response = self._rpc_request(action='createDeck', params=params)
        return response

    def delete_decks(self, decks_names: list[str], cards_too: bool = True) -> None:
        """
        Create a new empty deck. Will not overwrite a deck that exists with the same name.
        :param cards_too: Delete cards with in decks.
        :param decks_names: Decks name.
        :return: Deck ID.
        """
        params = {'decks': decks_names, "cardsToo": cards_too}
        response = self._rpc_request(action='deleteDecks', params=params)
        return response

    def get_deck_names_and_ids(self) -> dict:
        """
        Accepts an array of card IDs and returns an object with each deck name as a key,
        and its value an array of the given cards which belong to it.
        :return: Decks data as dict: deck name and its id.
        """
        response = self._rpc_request(action='deckNamesAndIds')
        return response

    def get_decks(self, cards_ids: list[int]) -> dict:
        """
        Accepts an array of card IDs and returns an object with each deck name as a key,
        and its value an array of the given cards which belong to it.

        :param cards_ids: Decks IDs.
        :return: Object with each deck name as a key, and its value an array of the given cards which belong to it.
        """
        params = {'cards': cards_ids}
        response = self._rpc_request(action='getDecks', params=params)
        return response

    def change_deck(self, deck_name: str, cards: list[int]) -> None:
        """
        Moves cards with the given IDs to a different deck, creating the deck if it doesn't exist yet.
        :param cards: Cards IDs.
        :param deck_name: Deck name.
        :return: Deck ID.
        """
        params = {'deck': deck_name, 'cards': cards}
        response = self._rpc_request(action='changeDeck', params=params)
        return response

    def add_note(self, note: AnkiNewNote) -> int:
        """
        Creates a note using the given deck and model, with the provided field values and tags.
        Returns the identifier of the created note created on success, and null on failure.
        :param note: Note.
        :return: Note ID.
        """
        params = {'note': self.json_converter.dump(note)}
        response = self._rpc_request(action='addNote', params=params)
        return response

    def add_notes(self, notes: list[AnkiNewNote]) -> list[int]:
        """
        Creates a note using the given deck and model, with the provided field values and tags.
        Returns the identifier of the created note created on success, and null on failure.

        - don't create new deck if deck not exist
        :param notes: Notes.
        :return: Notes IDs.
        """
        notes_js = [self.json_converter.dump(note) for note in notes]
        params = {'notes': notes_js}
        response = self._rpc_request(action='addNotes', params=params)

        notes_ids = [id_ for id_ in response if id_]  # remove None
        if len(notes_ids) < len(notes):
            logging.warning(msg=f'Anki add_notes returned not all notes IDs: notes count = {len(notes)}, '
                                f'notes_ids count = {len(notes_ids)}')
        return notes_ids

    def delete_notes(self, ids: list[int]) -> None:
        """
        Deletes notes with the given ids. If a note has several cards associated with it,
        all associated cards will be deleted.
        :param ids: Notes IDs.
        """
        params = {'notes': ids}
        response = self._rpc_request(action='deleteNotes', params=params)
        return response

    def notes_info(self, ids: list[int] | set[int]) -> list[AnkiNote]:
        """
        Returns a list of objects containing for each note ID the note fields, tags,
        note type and the cards belonging to the note.
        :param ids: Notes IDs.
        """
        params = {'notes': list(ids)}
        notes_info = self._rpc_request(action='notesInfo', params=params)
        notes = [self.json_converter.load(note, AnkiNote) for note in notes_info if note]
        return notes

    def find_cards(self, query: str) -> list[int]:
        """
        Returns an array of card IDs for a given query. Functionally identical to guiBrowse
        but doesn't use the GUI for better performance.
        https://docs.ankiweb.net/searching.html

        :param query: Cards IDs.
        """
        params = {'query': query}
        cards = self._rpc_request(action='findCards', params=params)
        return cards

    def find_notes(self, query: str) -> list[int]:
        """
        Returns an array of note IDs for a given query.
        https://docs.ankiweb.net/searching.html

        :param query: Notes IDs.
        """
        params = {'query': query}
        notes = self._rpc_request(action='findNotes', params=params)
        return notes

    def can_add_notes(self, notes: [AnkiNewNote]) -> list[bool]:
        """
        Accepts an array of objects [AnkiNote] which define parameters for candidate notes (see add_note) and returns
        an array of booleans indicating whether or not the parameters at the corresponding index could be used
        to create a new note.

        :param notes: Notes.
        :return: List of booleans indicating whether or not the note data at the corresponding index could be used
        to create a new note.
        """
        params = {'notes': [self.json_converter.dump(note) for note in notes]}
        result = self._rpc_request(action='canAddNotes', params=params)
        return result

    def get_decks_stats(self, decks: list[str]) -> dict[str: dict]:
        """
        Gets statistics such as total cards and cards due for the given decks.
        !!! Create deck, if not exists.

        :param decks: Decks names.
        :return: Object with each deck name as a key, and its value an array of the given cards which belong to it.
        """
        params = {'decks': decks}
        response = self._rpc_request(action='getDeckStats', params=params)
        return response

    def cards_info(self, ids: list[int]) -> list[dict]:  # TODO model
        """
        Returns a list of objects containing for each card ID the card fields, front and back sides including CSS,
        note type, the note that the card belongs to, and deck name,
        last modification timestamp as well as ease and interval.

        :param ids: Cards IDs.
        :return: Cards info.
        """
        params = {'cards': ids}
        cards = self._rpc_request(action='cardsInfo', params=params)
        return cards
