import re

from src.main.utils.logger_cfg import LoggingHandler


class TextParser(LoggingHandler):

    def text2words(self, text: list[str]) -> set[str]:
        """
        Extract words from text.
        Apostrophes in words will be replaced.
        Words with unknown apostrophes will be removed.
        Result words will contain only letters.

        Examples:
          Hello -> hello
          butt-probing -> butt, probing
          talkin' -> talking
          let's -> let
          wouldn't -> would
          'cause ->
          42 ->
          vas3k ->

        :param text: List of test strings.
        :return: Set of unique words in lowercase.
        """
        all_words = self._extract_words_from_text(text=text)
        valid_words = self.replace_apostrophes(words=all_words)

        return valid_words

    @staticmethod
    def _extract_words_from_text(text: list[str]) -> set[str]:
        """
        Extract words from text.

        Word may contain any letters and apostrophe.
        Before the word, there should be any character that is not a letter, digit, or apostrophe,
        or it should be the start of the string.
        After the word, there should be any character that is not a letter or digit,
        or it should be the end of the string.

        Examples:
          butt-probing -> butt, probing
          talkin' -> talkin'
          let's -> let's
          'cause -> 'cause
          42 ->
          vas3k ->

        For regex debug: https://regex101.com/
        :param text: Text.
        :return: Set of words in lowercase.
        """
        # TODO other language letters?
        # Word may contain any letters and apostrophe.
        word_body = "[a-zA-Z']+"  # "'?\w+'?"
        # Before the word, there should be any character that is not a letter, digit, or apostrophe,
        #  or it should be the start of the string.
        before_word = "(?:^|(?<=[^\w']))"  # перед словом слова следует любая не буква/цифра/апостроф или начало строки
        # After the word, there should be any character that is not a letter or digit,
        #  or it should be the end of the string.
        after_word = "(?:$|(?=[^\w]))"

        word_pattern = fr'{before_word}{word_body}{after_word}'

        words = set()
        for text_part in text:
            text_part_words = re.findall(pattern=word_pattern, string=text_part)
            new_words: set[str] = {word.lower() for word in text_part_words}
            words.update(new_words)

        return words

    def replace_apostrophes(self, words: set[str]) -> set[str]:
        """
        Checks words contain apostrophes and replaces those words with a valid words without apostrophe.
        Returns only valid words. If found word with unknown apostrophe, it will be excluded from valid words.
        :param words:
        :return: Unique words without apostrophe.
        """
        unidentified_word_with_apostrophe = set()
        valid_words = set()
        for word in words:
            valid_word = self._replace_apostrophe_in_word(word=word)
            if valid_word:
                valid_words.add(valid_word)
            else:
                unidentified_word_with_apostrophe.add(word)

        if unidentified_word_with_apostrophe:
            self.logger.debug(msg=f'Unidentified words with apostrophe: '
                                  f'{unidentified_word_with_apostrophe}')

        return valid_words

    def _replace_apostrophe_in_word(self, word: str) -> str | None:
        apostrophe = "'"

        contain_apostrophe = apostrophe in word
        if not contain_apostrophe:
            return word

        # end apostrophe
        if word[-1] == apostrophe:
            full_word = self._replace_end_apostrophe_in_word(word=word)
        # start apostrophe
        elif word[0] == apostrophe:
            full_word = self._replace_start_apostrophe_in_word(word=word)
        # mid apostrophe
        else:
            full_word = self._replace_mid_apostrophe_in_word(word=word)

        if full_word and apostrophe in full_word:  # for bugs and # TODO 'word'
            full_word = None

        return full_word

    @staticmethod
    def _replace_end_apostrophe_in_word(word: str) -> str | None:
        letters_count = len(word)

        # Plural possessive form, ex: two boxes' worth
        if letters_count > 2 and word[-2:-1] == 's':
            full_word = word[:-1]  # remove apostrophe
        # ing = in', ex: talkin' = talking
        elif letters_count > 3 and word[-3:-1] == 'in':
            full_word = word[:-1] + 'g'  # replace ' to g: in' -> ing
        else:
            full_word = None

        return full_word

    @staticmethod
    def _replace_mid_apostrophe_in_word(word: str) -> str | None:
        exceptions = {"can't": "can", "won't": "will", "you're": "you"}  # TODO: refactoring
        exception_word = exceptions.get(word)
        if exception_word:
            return exception_word

        letters_count = len(word)
        if letters_count > 2 and word[-2:] == "'s":
            full_word = word[:-2]  # remove 's: food's -> food
        elif letters_count > 3 and word[-3:] == "n't":  # wouldn't = would not
            full_word = word[:-3]  # remove n't: wouldn't -> would
        else:
            full_word = None

        return full_word

    @staticmethod
    def _replace_start_apostrophe_in_word(word: str) -> str | None:
        # TODO
        full_word = None  # EX: 'em = them, 'cause = because

        return full_word
