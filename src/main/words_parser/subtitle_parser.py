import pysrt

from src.main.words_parser.text_parser import TextParser


class SRTSubtitleParser(TextParser):

    def srt_subtitles2words(self, srt_file_path: str) -> set[str]:
        """
        Get words from .srt file.
        :param srt_file_path: Path to .srt file.
        :return: Set of unique words from subtitles.
        """
        text = self.srt2subtitles(srt_file_path=srt_file_path)
        words = self.text2words(text=text)
        return words

    @staticmethod
    def srt2subtitles(srt_file_path: str) -> list[str]:
        """
        Get text from .srt file.
        :param srt_file_path: Path to .srt file.
        :return: Subtitles text as list of strings.
        """
        srt_file = pysrt.open(path=srt_file_path)

        all_text = []
        for sub in srt_file:
            text = sub.text
            all_text.append(text)

        # TODO: close srt_file???

        return all_text



