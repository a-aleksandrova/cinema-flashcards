from src.main.dictionary_service.dictionary_service import DictionaryService
from src.main.flashcard_service.flashcard_sevice import FlashcardService
from src.main.nlp_service.nltk_nlp_service import NLPService
from src.main.utils.logger_cfg import LoggingHandler
from src.main.words_parser.subtitle_parser import SRTSubtitleParser


class CinemaFlashcardService(LoggingHandler):
    def __init__(self, flashcard_service: FlashcardService, dictionary: DictionaryService,
                 subs_parser: SRTSubtitleParser, nlp_service: NLPService):

        super().__init__()
        self.fsh_service = flashcard_service
        self.dictionary = dictionary
        self.subs_parser = subs_parser
        self.nlp_service = nlp_service
        self.keep_out_words = {'i', 'she', 'he', 'my', 'me', 'not', 'yes', 'no', 'it', 'of', 'at'}  # TODO: move to cfg
        self.logger.info(f'Keep out words: {self.keep_out_words}')

    def create_film_flashcards(self, srt_subs_file_path: str):
        raw_film_words = self.subs_parser.srt_subtitles2words(srt_file_path=srt_subs_file_path)
        self.logger.debug(f'Raw film words [{len(raw_film_words)}]: {raw_film_words}')

        all_film_words = self.nlp_service.get_base_words(words=raw_film_words, lowercase=True)
        self.logger.debug(f'All film words in base form [{len(all_film_words)}]: {all_film_words}')

        film_words = all_film_words.difference(self.keep_out_words)
        self.logger.info(f'Film words in base form [{len(film_words)}]: {film_words}')

        new_words = self.fsh_service.get_non_lexicon_words(words=film_words)
        self.logger.info(f'New words [{len(new_words)}]: {new_words}')

        if new_words:
            self.create_new_lexicon_flashcards(words=new_words)

        self.fsh_service.move_lexicon_words_to_film_deck(words=film_words)
        self.logger.info(f'Film deck created successfully.')

    def create_new_lexicon_flashcards(self, words: set[str]) -> None:
        """
        Create flashcards for words and add these flashcards to lexicon deck.
        :param words: Set of unique Words.
        """
        words_meaning = self.dictionary.get_words_meanings(words=list(words))
        new_flashcards = self.fsh_service.words_meaning2new_flashcards(words_data=words_meaning)
        self.fsh_service.add_new_lexicon_flashcards(flashcards=new_flashcards)
        self.logger.info(f'Created flashcards for new words: {len(new_flashcards)}.')

    def merge_film_deck_to_lexicon(self):  # for debug only TODO
        """
        Move all film deck flashcards to lexicon deck.
        Remove empty film deck.
        """
        self.fsh_service.merge_film_deck_to_lexicon()
        self.logger.info(f'Film deck merged to lexicon deck successfully.')

