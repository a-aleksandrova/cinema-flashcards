from dataclasses import dataclass
from typing import Literal

from adaptix import NameStyle, name_mapping, Retort
from requests import RequestException

from src.main.http_client import HTTPClient
from src.main.utils.logger_cfg import LoggingHandler
from temp_config import YANDEX_DICT_API_KEY


@dataclass
class YandexDictBaseModel:
    model_converter = Retort(
        recipe=[name_mapping(name_style=NameStyle.LOWER_SNAKE)])

    @classmethod
    def from_json(cls, json_data: dict):
        model_obg = cls.model_converter.load(json_data, cls)
        return model_obg

    def to_json(self):
        self.model_converter.dump(self)


@dataclass
class WordData(YandexDictBaseModel):
    text: str
    pos: str | None = None
    """Word part of speech"""
    gen: str | None = None
    """Gender"""
    fr: int | None = None
    """Frequency"""


@dataclass
class Example(YandexDictBaseModel):
    text: str
    """Example in source lang"""
    tr: list[WordData]
    """Example translations to target lang"""


@dataclass
class YandexTranslation(YandexDictBaseModel):
    text: str
    """Translated word"""
    pos: str | None = None
    """Translated word part of speech"""
    fr: int | None = None
    """Frequency"""
    gen: str | None = None
    """Gender"""
    syn: list[WordData] | None = None
    """Synonym in target lang"""
    mean: list[WordData] | None = None
    """Meaning in source lang"""
    ex: list[Example] | None = None
    """Example in source and target lang"""


@dataclass
class YandexDefinition(YandexDictBaseModel):
    text: str
    """Source word"""
    tr: list[YandexTranslation]
    """Word translation data"""
    pos: str | None = None
    """Source word part of speech"""
    ts: str | None = None
    """Word transcription"""


@dataclass
class LookupResult(YandexDictBaseModel):
    head: dict
    """Always empty dict"""
    def_: list[YandexDefinition]
    """Word definitions data"""


class YandexDictionaryAPI(LoggingHandler):

    def __init__(self):
        super().__init__()
        url = 'https://dictionary.yandex.net/api/v1/dicservice'
        # 'https://yandex.com/dev/dictionary/'  # TODO move to cfg
        self.api_key: str = YANDEX_DICT_API_KEY
        self.client = HTTPClient(url=url)
        self.model_converter = Retort(
            recipe=[name_mapping(name_style=NameStyle.LOWER_SNAKE)]
        )

    def _get_json(self, uri: str, params: dict = None):
        # Add api key
        params = params or {}
        params['key'] = self.api_key
        headers = {'Content-Type': 'application/json'}

        # Send get request with json result
        resp = self.client.get(uri='.json' + uri, params=params, headers=headers)

        # Check no errors
        code = resp.status_code
        if code not in [200]:
            raise RequestException(f'YandexDictionaryAPI request failed. Req: {resp.request.url, params},'
                                   f'\nResp: {resp.text}')  # TODO

        # Convert response to json
        response = resp.json()

        return response

    def lookup(self, lang: Literal['en-ru'], text: str, ui: str = None,
               flags: Literal['FAMILY', 'MORPHO', 'POS_FILTER'] = None) -> LookupResult:
        """
        Searches for a word or phrase in the dictionary and returns an automatically generated dictionary entry.

        Tip. The service does not just work with translation dictionaries. For example, you can set
        the direction "ru-ru" or "en-en" to get all the possible meanings of the word you are searching for
        in Russian or English.

        :param lang: Translation direction (for example, "en-ru"). Set as a pair of language codes separated by
        a hyphen. For example, "en-ru" specifies to translate from English to Russian.
        :param text: The word or phrase to find in the dictionary.
        :param flags: The language of the user's interface for displaying names of parts of speech
        in the dictionary entry.
        :param ui: Search options (bitmask of flags).
        Possible values:
        FAMILY = 0x0001 - Apply the family search filter.
        MORPHO = 0x0004 - Enable searching by word form.
        POS_FILTER = 0x0008 - Enable a filter that requires matching parts of speech for the search word
        and translation.

        :return: Dictionary entry that is generated automatically based on search results.
        """
        flags_data = {'FAMILY': 0x0001, 'MORPHO': 0x0004, 'POS_FILTER': 0x0008}
        flags_ = flags_data.get(flags)
        params = {'lang': lang,
                  'text': text,
                  'ui': ui,
                  'flags': flags_}
        response_json = self._get_json(uri='/lookup', params=params)
        response = LookupResult.from_json(json_data=response_json)
        return response
