from typing import Literal
from requests import RequestException

from src.main.dictionary_service.dictionary_service import BilingualDictionary, TargetLangData, TargetLangTranslation
from src.main.dictionary_service.yandex_dictionary.yandex_dictionary_api import YandexDictionaryAPI, YandexTranslation


class YandexDictionaryService(BilingualDictionary):
    """Bilingual source-target language dictionary"""
    name = 'YandexDictionaryService'

    def __init__(self, lang: Literal['en-ru'] = 'en-ru'):
        """
        :param lang: Source-target language pair.
        """
        super().__init__(lang)
        self.api = YandexDictionaryAPI()
        self.max_per_part_of_speech: int = 7  # max translations per part of speech TODO: move to cfg?
        self.min_translation_frequency = 5  # TODO: move to cfg?

    def get_words_target_lang_data(self, words: list[str]) -> list[TargetLangData]:
        """
        Get words meanings in target lang.
        :param words: Words in source lang.
        :return: Words meanings in target lang.
        """
        words_meanings = []
        not_found_words = set()
        for word in words:
            word_meaning = self._get_word_meaning(word=word)
            if word_meaning:
                words_meanings.append(word_meaning)
            else:
                not_found_words.add(word)

        if not_found_words:
            self.logger.info(f'Definitions not found: lang={self.lang}, '
                             f'min_translation_frequency={self.min_translation_frequency}, words={not_found_words}')
            # extra={'lang': self.lang, 'min_translation_frequency': self.min_translation_frequency, 'words':
            # not_found_words}

        return words_meanings

    def _get_word_meaning(self, word: str) -> TargetLangData | None:
        """
        Get word meaning in target language.
        :param word: Word in source language.
        :return: Word meaning or None if meaning not found.
        """
        # Get all word definitions from Dictionary
        try:
            word_definitions_data = self.api.lookup(text=word, lang=self.lang)
        except RequestException:
            self.logger.error(msg=f'API request failed for word={word} with error.', exc_info=True)
            return None

        definitions = word_definitions_data.def_
        # Check definitions exist
        if not definitions:
            return None

        # Get translations per part of speech
        all_translations_data = []
        definitions_without_pos = []
        for definition in definitions:
            part_of_speech, translations = definition.pos, definition.tr
            if part_of_speech:
                all_translations_data.extend(translations)
            else:
                definitions_without_pos.append(definition)

        selected_translations = self._pick_translations(translations_data=all_translations_data)
        if not selected_translations:
            return None

        if definitions_without_pos:
            self.logger.debug(msg=f"Received definitions without pos for word={word}: " +
                                  '\n'.join([str(d) for d in definitions_without_pos]))

        # Get transcription
        transcription = definitions[0].ts if definitions else None
        # TODO: use IPA transcription from monolingual source language dictionary

        # Get main transcription with the biggest frequency
        main_translation = selected_translations[0] if selected_translations else None

        word_meaning = TargetLangData(word=word,
                                      main_translation=main_translation,
                                      translations=selected_translations,
                                      transcription=transcription)
        return word_meaning

    def _pick_translations(self, translations_data: list[YandexTranslation]) -> list[TargetLangTranslation]:
        """
        Select word translations and sort by frequency.
        :param translations_data: Translations from Yandex Dictionary.
        :return: Selected translations.
        """
        selected_translations = []
        translations_without_pos = []
        for translation_data in translations_data:
            translation_frequency = translation_data.fr or 0  # word translation frequency, 0 if None
            # selected translations count not max and translation frequency not lower than min_translation_frequency
            if (translation_frequency >= self.min_translation_frequency
                    and len(selected_translations) < self.max_per_part_of_speech):
                pos = translation_data.pos
                if pos:
                    # Add new translation
                    new_translation = TargetLangTranslation(word=translation_data.text,
                                                            # word translation in target lang
                                                            part_of_speech=pos,
                                                            fr=translation_frequency)
                    selected_translations.append(new_translation)

                else:
                    translations_without_pos.append(translation_data)

        if translations_without_pos:
            self.logger.debug(msg=f'Received translations without pos: ' +
                                  '\n'.join([str(tr) for tr in translations_without_pos]))

        selected_translations.sort(key=lambda translation: translation.fr, reverse=True)  # sort by frequency descending

        return selected_translations
