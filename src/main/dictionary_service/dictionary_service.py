import logging
from dataclasses import dataclass
from typing import Literal

from src.main.utils.logger_cfg import LoggingHandler


@dataclass
class TargetLangTranslation:
    word: str
    """Word in target lang"""
    part_of_speech: str
    """Word part of speech"""
    fr: int | None
    """Translation frequency"""


@dataclass
class TargetLangData:
    word: str
    """Word in source lang"""
    main_translation: TargetLangTranslation
    """Main word translation in target lang"""
    translations: list[TargetLangTranslation]
    """ Word translations in target lang"""
    transcription: str | None


@dataclass
class SourceLangData:  # TODO
    transcription: str
    meanings: list
    audio: str
    examples: list


@dataclass
class WordMeaning:
    word: str
    target_lang_meaning: TargetLangData
    source_lang_meaning: SourceLangData | None = None
    transcription: str | None = None
    lang: str = None


class SourceLangDictionary(LoggingHandler):
    name: str

    def get_words_source_lang_data(self, words: list[str]) -> list[SourceLangData]:
        pass


class BilingualDictionary(LoggingHandler):
    name: str

    def __init__(self, lang: Literal['en-ru'] = 'en-ru'):
        super().__init__()
        self.lang = lang

    def get_words_target_lang_data(self, words: list[str]) -> list[TargetLangData]:
        pass


class DictionaryService(LoggingHandler):

    def __init__(self, bilingual_dictionary: BilingualDictionary, source_lang_dictionary: SourceLangDictionary = None):
        super().__init__()
        self.bilingual_dictionary = bilingual_dictionary
        self.source_lang_dictionary = source_lang_dictionary
        self.logger.info(f'Configured dictionary services: '
                         f'bilingual dictionary ({self.bilingual_dictionary.name}, '
                         f'land={self.bilingual_dictionary.lang}),  '
                         # f'monolingual dictionary ({self.source_lang_dictionary.name}, '  # TODO
                         # f'lang={self.source_lang_dictionary.lang})'
                         )

    def get_words_meanings(self, words: list[str]) -> list[WordMeaning]:  # TODO return NewFlashcard
        words_target_lang_data = self.bilingual_dictionary.get_words_target_lang_data(words=words)
        # words_source_lang_data = self.source_lang_dictionary.get_words_source_lang_data(words=words)  # TODO

        words_meanings = []
        word_without_meaning = set()
        for word, target_lang_data in zip(words,
                                          words_target_lang_data,
                                          # words_source_lang_data, TODO
                                          strict=True):
            if target_lang_data.translations:  # or words_source_lang_data  # TODO refactoring
                word_meaning = WordMeaning(word=word,
                                           source_lang_meaning=None,
                                           target_lang_meaning=target_lang_data,
                                           transcription=target_lang_data.transcription,  # or source_lang_data.transcription TODO
                                           lang=self.bilingual_dictionary.lang)  # or source_lang_data.lang TODO
                words_meanings.append(word_meaning)
            else:
                word_without_meaning.add(word)

        if word_without_meaning:
            self.logger.info(msg=f'Dictionary data not found for words: {word_without_meaning}')

        return words_meanings
